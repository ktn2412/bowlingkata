﻿using System;

namespace BowlingGame
{
    public class Bowling
    {
        private readonly int[] _roll = new int[21];
        private int _currentRollIndex;
        public void RollBall(int pin)
        {
            _roll[_currentRollIndex++] += pin; //the score for frames is the total number of pins knocked down
        }
        public void RollBallArray(int[] pin)
        {
            for (int i = 0; i < pin.Length; i++)
            {
                _roll[_currentRollIndex++] += pin[i]; //the score for frames is the total number of pins knocked down    
            }
        }
        private int CalculateScore()
        {
            var score = 0;
            var rollNumber = 0;
            for (var currentFrame = 0; currentFrame <= 10; currentFrame++)
            { 
                if (_roll[rollNumber] == 10) //A Strike is when the player knocks down all 10 pins on his first try.
                {
                    score += 10 + _roll[rollNumber + 1] + _roll[rollNumber + 2];//The bonus for that frame is the value of the next two ball rolled (rollNumber++)
                    rollNumber++;
                }
                else if (_roll[rollNumber] + _roll[rollNumber + 1] == 10) //Is Spare is when the player knocked down all 10 pins, in two tires.
                {
                    score += 10 + _roll[rollNumber + 2]; //The bonus for that frame is the number of pins knocked down by the next roll.
                    rollNumber += 2;
                }
                else
                {
                    score += _roll[rollNumber] + _roll[rollNumber + 1]; //Score without Spare or Strike 
                    rollNumber += 2;
                }
            }
            return score;
        }
        public int TotalScore
        {
            get => CalculateScore();

        }

    }
}
