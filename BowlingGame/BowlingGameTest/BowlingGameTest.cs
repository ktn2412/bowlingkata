using BowlingGame;
using Xunit;

namespace BowlingGameTest
{
    
    public class BowlingGameTest
    {
        //arrange
        private readonly Bowling _game;
        public BowlingGameTest()
        {
                _game = new Bowling();
        }
        private void CanRollBall(int balls, int pins)
        {
            for (int i = 0; i < balls; i++)
            {
                _game.RollBall(pins);
            }
        }

        private void IsSpare()
        {
            var firstTry = 6;
            var secondTry = 4;
            _game.RollBall(firstTry);
            _game.RollBall(secondTry);
        }

        private void IsStrike()
        {
            var firstTry = 10;
            _game.RollBall(firstTry);
        }
        [Fact]
        public void TestGameIsTypeBowling()
        {
            //assert
            Assert.IsType<Bowling>(_game);
        }
        [Fact]
        public void TestMiss()
        {
            _game.RollBall(0);
            CanRollBall(20, 0);
            //assert
            Assert.Equal(0,_game.TotalScore);
        }
        [Fact]
        public void TestIfKnockedOne()
        {
            _game.RollBall(1);
            CanRollBall(20, 1);
            //assert
            Assert.Equal(20, _game.TotalScore);
        }
        [Fact]
        public void TestIfSpare()
        {
            IsSpare();
            var firstTryNextFrame = 8;
            _game.RollBall(firstTryNextFrame);
            CanRollBall(17,0);
            //assert
            Assert.Equal(26, _game.TotalScore);
        }
        [Fact]
        public void TestIfStrike()
        {
            var firstTryNextFrame = 3;
            var secondTryNextFrame = 4;
            IsStrike();//Strike
            _game.RollBall(firstTryNextFrame);
            _game.RollBall(secondTryNextFrame);
            CanRollBall(16,0);
            Assert.Equal(24,_game.TotalScore);
        }
        [Fact]
        public void TestIfStrikeInTenthFrame()
        {
            _game.RollBallArray(new int[] {3,7,3,2,4,5,0,6,5,4,3,2,2,4,7,3,3,0,10,8}); //20 balls 
            Assert.Equal(95, _game.TotalScore);
        }
        [Fact]
        public void TestIfSpareInTenthFrame()
        {
            _game.RollBallArray(new int[] {3,7,3,2,4,5,0,6,5,4,3,2,2,4,7,3,3,7,10 }); 
            Assert.Equal(96, _game.TotalScore);
        }
    }
}
